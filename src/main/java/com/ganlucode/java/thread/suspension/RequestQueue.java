package com.ganlucode.java.thread.suspension;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author GanLu
 * @date 2020-06-09 20:54
 */
public class RequestQueue {

    private final Queue<Request> queue = new LinkedList<>();

    public synchronized Request getRequest(){
        while ((queue.peek() == null)) {
            try {
                System.out.println("等待放入队列");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("获取队列");
        return queue.remove();
    }

    public synchronized void putRequest(Request request) {
        System.out.println("放入队列");
        queue.offer(request);
        notifyAll();
    }


}
