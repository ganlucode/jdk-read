package com.ganlucode.java.thread.wait;

/**
 * @author GanLu
 * @date 2020-06-02 11:13
 */
public class WaitNotifyRunnable implements Runnable {

    private String name;

    private WaitNotifyObject object;

    public WaitNotifyRunnable() {
    }

    public WaitNotifyRunnable(String name, WaitNotifyObject object) {
        this.name = name;
        this.object = object;
    }

    @Override
    public void run() {
        if (object != null) {
            object.run(name);
        }
    }


}