package com.ganlucode.java.thread.wait;

/**
 * @author GanLu
 * @date 2020-06-02 11:10
 */
public class WaitNotifyObject {


    public synchronized void run(String name) {
        System.out.println(String.format("Runnable is running, name:%s",name));
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("Runnable is notified, name:%s",name));
    }

    public synchronized void freeOne(){
        notify();
    }

    public synchronized void freeAll(){
        notifyAll();
    }

}
