package com.ganlucode.java.thread.utils;

import java.util.Random;

/**
 * @author GanLu
 * @date 2020-09-08 19:55
 */
public final class TestUtils {
    private TestUtils() {
    }

    //length用户要求产生字符串的长度
    public static String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

}
