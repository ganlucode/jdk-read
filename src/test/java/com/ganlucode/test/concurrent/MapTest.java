package com.ganlucode.test.concurrent;

import com.ganlucode.java.thread.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author GanLu
 * @date 2020-09-08 19:45
 */
public class MapTest {

    private ExecutorService executor;
    private Map<String, Object> map;

    @Before
    public void before(){
//        Integer corePoolSize = 4;
//        Integer maximumPoolSize = 4;
//        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
//        executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
//                0L, TimeUnit.MILLISECONDS, workQueue);
        executor = Executors.newFixedThreadPool(4);
        map = new ConcurrentHashMap<>();
    }

    @Test
    public void mapTest() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            executor.submit(() -> {
                String key = TestUtils.getRandomString(5);
                String value = TestUtils.getRandomString(5);
                map.put(key, value);
            });
        }
        Thread.currentThread().sleep(60*1000);
    }

}
