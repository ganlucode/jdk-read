package com.ganlucode.test.thread;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * @author GanLu
 * @date 2020-06-02 20:42
 */
public class SemaphoreTest {

    private ExecutorService threadPoolExecutor;
    private Semaphore semaphore;

    @Before
    public void before(){
        Integer corePoolSize = 3;
        Integer maximumPoolSize = 3;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(1);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
        semaphore = new Semaphore(2);
    }

    @Test
    public void test() throws InterruptedException {
        threadPoolExecutor.submit(new TestRunable("A", semaphore));
//        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new TestRunable("B", semaphore));
//        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new TestRunable("C", semaphore));
        Thread.sleep(7 * 1000);


    }

    @AllArgsConstructor
    @NoArgsConstructor
    private class TestRunable implements Runnable {

        private String name;

        private Semaphore semaphore;


        @Override
        public void run() {
            System.out.println(String.format("%s :start",name));
            try {
                semaphore.acquire();
                System.out.println(String.format("%s :semaphore acquired",name));
                Thread.sleep(3 * 1000);
                semaphore.release();
                System.out.println(String.format("%s :semaphore released",name));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
