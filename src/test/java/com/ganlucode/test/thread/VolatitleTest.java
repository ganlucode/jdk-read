package com.ganlucode.test.thread;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * @author GanLu
 * @date 2020-06-30 09:09
 */
public class VolatitleTest {

    private ExecutorService threadPoolExecutor;

    @Before
    public void before(){
        Integer corePoolSize = 4;
        Integer maximumPoolSize = 8;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
    }

    @Test
    public void test() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            VolatitleRunner runner = new VolatitleRunner(i);
            threadPoolExecutor.submit(runner);
            Thread.sleep(50);
            runner.shutdown();
            Thread.sleep(50);
        }
    }

    @Test
    public void test2() throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            ReadWriteRunner runner = new ReadWriteRunner();
            threadPoolExecutor.submit(runner);
//            Thread.sleep(50);
            runner.read();
//            Thread.sleep(100);
        }
    }

    @Test
    public void test3() throws InterruptedException {
        ReadWriteObject readWriteObject = new ReadWriteObject();
        threadPoolExecutor.submit(()->{
            while (true) {
                readWriteObject.read();
            }
        });
        for (int i = 0; i < 100; i++) {
            int finalI = i;
            threadPoolExecutor.submit(()->{
                    readWriteObject.write(finalI);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            });
        }
        Thread.sleep(10*1000);
    }

    class VolatitleRunner implements Runnable{

        private boolean quit = false;
//        private volatile boolean quit = false;

        private int index;

        public VolatitleRunner(int index) {
            this.index = index;
        }

        public void shutdown(){
            this.quit = true;
        }

        @Override
        public void run() {
            System.out.println("Start " + index);
            while (!quit) {

            }
            System.out.println("End " + index);
        }
    }

    class ReadWriteRunner implements Runnable{
        private int x = 0;
        private int y = 0;

        public void write(){
            x = 100;
            y = 50;
        }

        public void read(){
            if (x < y) {
                System.out.println("x<y");
            }
        }

        @Override
        public void run() {
            write();
        }
    }
    class ReadWriteObject{
        private int x = 0;
        private volatile boolean valid = false;

        public void write(int x) {
            this.x = x;
            valid = true;
        }

        public void read(){
            if (valid) {
                System.out.println(x);
                valid = false;
            }
        }
    }

}
