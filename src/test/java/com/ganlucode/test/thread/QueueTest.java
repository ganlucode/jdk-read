package com.ganlucode.test.thread;

import org.junit.Test;

/**
 * @author GanLu
 * @date 2020-06-10 18:12
 */
public class QueueTest {


    @Test
    public void test() throws InterruptedException {
        Object o = new Object();
        synchronized (o) {
            o.wait(2000);
        }
    }
}
