package com.ganlucode.test.thread;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author GanLu
 * @date 2020-06-30 10:17
 */
public class ReadWriteTest {

    private ExecutorService threadPoolExecutor;

    @Before
    public void before(){
        Integer corePoolSize = 4;
        Integer maximumPoolSize = 8;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
    }

    @Test
    public void test() throws InterruptedException {
        TestTask testTask = new TestTask();
        threadPoolExecutor.submit(()->{
            int i = 1;
            while (true) {
                try {
                    testTask.write(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
        });
        threadPoolExecutor.submit(()->{
            while (true) {
                try {
                    testTask.read();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread.sleep(20*1000);
    }

    class TestTask{
        private int num = 0;
        private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        public void write(int num) throws InterruptedException {
            readWriteLock.writeLock().lock();
            System.out.println("writeLock");
            this.num = num;
            Thread.sleep(900);
            readWriteLock.writeLock().unlock();
            Thread.sleep(100);
        }
        public void read() throws InterruptedException {
            readWriteLock.readLock().lock();
            System.out.println("readLock");
            System.out.println(num);
            Thread.sleep(900);
            readWriteLock.readLock().unlock();
//            Thread.sleep(100);
        }
    }
}
