package com.ganlucode.test.thread;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author GanLu
 * @date 2020-06-04 15:25
 */
public class PerformanceTest {
    private ExecutorService threadPoolExecutor;
    private Lock lock;
    private Integer runCounts;
    private AtomicInteger finishCount;
    private AtomicInteger validTaskCount;
    private byte type;

    @Before
    public void before(){
        Integer corePoolSize = 4;
        Integer maximumPoolSize = 4;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
        lock = new ReentrantLock();
//        lock = new ReadLo();
        runCounts = 100000;
        finishCount = new AtomicInteger(0);
        validTaskCount = new AtomicInteger(runCounts);
//        type = TestRunable.TYPE_SYNCHRONIZED;
        type = TestRunable.TYPE_LOCK;
    }

    @Test
    public void test(){
        Long start = System.currentTimeMillis();
        for (int i = 0; i < runCounts; i++) {
            threadPoolExecutor.execute(new TestRunable(lock,type));
        }
        while (true) {
            if (finishCount.get() >= validTaskCount.get()) {
                break;
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("Total Time:"+(end-start)+"millisecond");
    }


    private class TestRunable implements Runnable {

        final static byte TYPE_SYNCHRONIZED = 1;
        final static byte TYPE_LOCK = 2;
        private Lock lock;
        private byte type;

        public TestRunable() {
        }

        public TestRunable(Lock lock, byte type) {
            this.lock = lock;
            this.type = type;
        }

        @Override
        public void run() {
            System.out.println(String.format("%s :start",Thread.currentThread().getName()));
            if (type == TYPE_SYNCHRONIZED){
                synchronized (lock){
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                    System.out.println(String.format("%s :synchronized",Thread.currentThread().getName()));
                }
            }else if(type == TYPE_LOCK){
                try {
                    lock.lock();
                    System.out.println(String.format("%s :lock",Thread.currentThread().getName()));
                }finally {
                    lock.unlock();
                }
            }

            System.out.println(String.format("%s :end,finishCount:%s",Thread.currentThread().getName(),finishCount.incrementAndGet()));
        }
    }

}
