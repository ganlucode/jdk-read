package com.ganlucode.test.thread;

import com.ganlucode.java.thread.wait.WaitNotifyObject;
import com.ganlucode.java.thread.wait.WaitNotifyRunnable;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * @author GanLu
 * @date 2020-06-01 23:03
 */
public class WaitNotifyTest {
    private ExecutorService threadPoolExecutor;
    private Object object;

    @Before
    public void before(){
        Integer corePoolSize = 4;
        Integer maximumPoolSize = 4;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(100);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
        object = new Object();
    }


    @Test
    public void test() throws InterruptedException {
        threadPoolExecutor.submit(new TestRunable("A", object));
        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new TestRunable("B", object));
        Thread.sleep(2 * 1000);
        synchronized (object) {
            object.notify();
        }
        Thread.sleep(2 * 1000);
        synchronized (object) {
            object.notify();
        }
    }

    @Test
    public void test2() throws InterruptedException {

        threadPoolExecutor.submit(new TestRunable("A", object));
        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new TestRunable("B", object));
        Thread.sleep(2 * 1000);
        synchronized (object) {
            object.notifyAll();
        }
    }

    @Test
    public void test3() throws InterruptedException {
        WaitNotifyObject object = new WaitNotifyObject();
        threadPoolExecutor.submit(new WaitNotifyRunnable("A", object));
        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new WaitNotifyRunnable("B", object));
        Thread.sleep(2 * 1000);
        object.freeOne();
        Thread.sleep(2 * 1000);
        object.freeAll();
    }
    @Test
    public void test4() throws InterruptedException {
        WaitNotifyObject object = new WaitNotifyObject();
        threadPoolExecutor.submit(new WaitNotifyRunnable("A", object));
        Thread.sleep(2 * 1000);
        threadPoolExecutor.submit(new WaitNotifyRunnable("B", object));
        Thread.sleep(2 * 1000);
        object.freeAll();
//        object.notifyAll();
    }

    @Test
    public void test5() throws InterruptedException {
        TestObject testObject = new TestObject();
        for (int i = 0; i < 4; i++) {
            threadPoolExecutor.submit(()->{
                try {
                    testObject.run(Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        for (int i = 0; i < 4; i++) {
            Thread.sleep(1000);
            testObject.free();
        }

    }

    public class TestRunable implements Runnable {

        private String name;
        private Object object;

        public TestRunable(String name, Object object) {
            this.name = name;
            this.object = object;
        }

        public TestRunable() {
        }

        @Override
        public void run() {
            System.out.println(String.format("Runnable is running, name:%s",name));
            try {
                synchronized (object) {
                    object.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("Runnable is notified, name:%s",name));
        }
    }
    public class TestObject {

        public synchronized void run(String name) throws InterruptedException {
            System.out.println("进入synchronized方法，name:" + name);
            wait();
            System.out.println("wait结束，name:" + name);
        }

        public synchronized void free(){
            notify();
        }
    }

    @Test
    public void testWait(){
        synchronized (this){
            try {
                wait(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
