package com.ganlucode.test.thread;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author GanLu
 * @date 2020-05-19 19:58
 */
public class ThreadPoolExecutorTest {

    private ExecutorService threadPoolExecutor;
    private Integer taskCount;
    private AtomicInteger finishCount;
    private AtomicInteger validTaskCount;

    @Before
    public void before(){
        Integer corePoolSize = 2;
        Integer maximumPoolSize = 2;
        taskCount = 4;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(1);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
        finishCount = new AtomicInteger(0);
        validTaskCount = new AtomicInteger(taskCount);
    }


    @Test
    public void test() throws InterruptedException {
        long start = System.currentTimeMillis();
        for (int i = 0; i < taskCount; i++) {
            try {
                threadPoolExecutor.submit(new TestRunable());
            } catch (RejectedExecutionException e) {
                validTaskCount.decrementAndGet();
                System.out.println(String.format("validTaskCount:%s", validTaskCount.get()));
                e.printStackTrace();
            }
        }
        while (true) {
            if (finishCount.get() >= validTaskCount.get()) {
                break;
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("finish");
        System.out.println(String.format("time:%d", (end-start)/1000));
    }



    private class TestRunable implements Runnable {

        @Override
        public void run() {
            System.out.println(String.format("%s :start",Thread.currentThread().getName()));
            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("%s :end",Thread.currentThread().getName()));
            finishCount.incrementAndGet();
        }
    }

}
