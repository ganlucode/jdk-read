package com.ganlucode.test.thread;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author GanLu
 * @date 2020-06-30 10:01
 */
public class SynchronizedTest {

    private ExecutorService threadPoolExecutor;

    @Before
    public void before(){
        Integer corePoolSize = 4;
        Integer maximumPoolSize = 8;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                0L, TimeUnit.MILLISECONDS, workQueue);
    }

    @Test
    public void test() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            TestTask testTask = new TestTask();
            int finalI = i;
//            testTask.write(finalI);
            threadPoolExecutor.submit(()->{
                testTask.write(finalI);
//                testTask.read();
            });
            threadPoolExecutor.submit(()->{
                testTask.read();
            });
//            testTask.read();
            Thread.sleep(10);
        }
    }



    class TestTask{
        private int num = 0;
        private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        public void write(int num) {
            readWriteLock.writeLock().lock();
            this.num = num;
            readWriteLock.writeLock().unlock();
        }
        public void read(){
            readWriteLock.readLock().lock();
            System.out.println(num);
            readWriteLock.readLock().unlock();
        }
    }

}
